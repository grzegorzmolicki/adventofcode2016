package com.molicki.day8;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class DayEight {
	final static int COLUMNS = 64;
	final static int ROWS = 64;

	private static void drawPoint(int x, int y, char c) {
		char escCode = 0x1B;
		int offsetX = 10;
		int offsetY = 10;
		System.out.print(String.format("%c[%d;%df",escCode,offsetY+y,offsetX+x));
		System.out.print(c);
	}

	private static void drawMatrix(boolean[][] matrix) {
		for(int y = 0; y < ROWS; y++) {
			for(int x = 0; x < COLUMNS; x++) {
				if(matrix[y][x])
					drawPoint(x, y, '*');
				else
					drawPoint(x, y, ' ');
			}
		}
	}

	public static void daily() throws FileNotFoundException, InterruptedException {
		boolean[][] matrix = new boolean[ROWS][COLUMNS];
		Scanner in = new Scanner(new File("C:\\Users\\Grzegorz\\Documents\\java\\AdventOfCode\\day8.txt"));

		drawPoint(-2, -2, ' ');
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();


		while(in.hasNextLine()) {
			String line = in.nextLine();
			String[] separated = line.split(" ");
			if(separated[0].equals("rect")) {
				// light rectangle X x Y
				String[] xy = separated[1].split("x");
				for(int x = 0; x < Integer.valueOf(xy[0]); x++) {
					for(int y = 0; y < Integer.valueOf(xy[1]); y++) {
						matrix[y][x] = true;
					}
				}
			} else {
				int value = Integer.valueOf(separated[4]);
				if(separated[1].equals("row")) {
					int rowIndex = Integer.valueOf(separated[2].substring(2));
					boolean[] newRow = new boolean[COLUMNS];
					for(int i = 0; i < COLUMNS; i++) {
						if(matrix[rowIndex][i]) {
							int newPosition = (i+value)%COLUMNS;
							newRow[newPosition] = true;
						}
					}
					matrix[rowIndex] = newRow;
				} else {
					int columnIndex = Integer.valueOf(separated[2].substring(2));
					boolean[] newColumn = new boolean[ROWS];
					for(int i = 0; i < ROWS; i++) {
						if(matrix[i][columnIndex]) {
							int newPosition = (i+value)%ROWS;
							newColumn[newPosition] = true;
						}
					}

					for(int i = 0; i < ROWS; i++) {
						matrix[i][columnIndex] = newColumn[i];
					}
				}
			}
			drawMatrix(matrix);
			Thread.sleep(50);
		}
		System.out.println();
		System.out.println();
		System.out.println();

		int counter = 0;
	}
}
