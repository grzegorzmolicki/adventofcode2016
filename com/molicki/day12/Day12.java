package com.molicki.day12;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Day12 {

	private static final int INPUT_LENGTH = 23;

	public static void daily() {
		ArrayList<String> code = new ArrayList<>(INPUT_LENGTH);
		Scanner in = new Scanner(System.in);

		for(int i = 0; i < INPUT_LENGTH; i++) {
			code.add(in.nextLine());
		}

		Map<Character, Integer> registers = new HashMap<>();
		registers.put('a', 0);
		registers.put('b', 0);
		registers.put('c', 1);
		registers.put('d', 0);

		int lineCounter = 0;

		while(lineCounter < INPUT_LENGTH) {
			String[] separated = code.get(lineCounter).split(" ");
			Integer registerValue;
			switch (separated[0]) {
				case "cpy":
					String from = separated[1];
					String to = separated[2];
					int value;

					if(from.length() == 1 && Character.isAlphabetic(from.charAt(0))) {
						value = registers.get(from.charAt(0));
					} else {
						value = Integer.valueOf(from);
					}

					registers.put(to.charAt(0), value);
					lineCounter++;
					break;
				case "inc":
					registerValue = registers.get(separated[1].charAt(0));
					registerValue++;
					registers.put(separated[1].charAt(0), registerValue);
					lineCounter++;
					break;
				case "dec":
					registerValue = registers.get(separated[1].charAt(0));
					registerValue--;
					registers.put(separated[1].charAt(0), registerValue);
					lineCounter++;
					break;
				case "jnz":
					if(separated[1].length() == 1 && separated[1].charAt(0) > 'a' && separated[1].charAt(0) < 'e') {
						if(registers.get(separated[1].charAt(0)) != 0) {
							lineCounter += Integer.valueOf(separated[2]);
						} else {
							lineCounter++;
						}
					} else {
						if(Integer.valueOf(separated[1]) != 0) {
							lineCounter += Integer.valueOf(separated[2]);
						} else {
							lineCounter++;
						}
					}
					break;
			}
		}
		System.out.println("A: " + registers.get('a'));
	}
}
