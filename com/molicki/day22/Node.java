package com.molicki.day22;

public class Node {
	int x;
	int y;
	String address;
	int capacity;
	int used;
	int available;
	int usePercentage;

	public Node() {
	}

	public String getAddress() {
		return address;
	}

	public Node setAddress(String address) {
		String[] separated = address.split("-");
		this.x = Integer.valueOf(separated[1].substring(1));
		this.y = Integer.valueOf(separated[2].substring(1));
		this.address = address;
		return this;
	}

	public int getCapacity() {
		return capacity;
	}

	public Node setCapacity(int capacity) {
		this.capacity = capacity;
		return this;
	}

	public int getUsed() {
		return used;
	}

	public Node setUsed(int used) {
		this.used = used;
		return this;
	}

	public int getAvailable() {
		return available;
	}

	public Node setAvailable(int available) {
		this.available = available;
		return this;
	}

	public int getUsePercentage() {
		return usePercentage;
	}

	public Node setUsePercentage(int usePercentage) {
		this.usePercentage = usePercentage;
		return this;
	}

	@Override
	public String toString() {
		return used+"/"+capacity;
	}
}
