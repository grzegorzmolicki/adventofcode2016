package com.molicki.day22;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Day22 {

	private static final int INPUT_LENGTH = 896;

	public static void dailyPart1() {

		Scanner in = new Scanner(System.in);
		List<Node> nodes = new ArrayList<>(896);

		for(int i = 0; i < INPUT_LENGTH; i++) {
			String[] separated = in.nextLine().split("\\s+");
			nodes.add(new Node().setAddress(separated[0])
				.setCapacity(Integer.valueOf(separated[1].substring(0, separated[1].length()-1)))
				.setUsed(Integer.valueOf(separated[2].substring(0, separated[2].length()-1)))
				.setAvailable(Integer.valueOf(separated[3].substring(0, separated[3].length()-1)))
				.setUsePercentage(Integer.valueOf(separated[4].substring(0, separated[4].length()-1))));
		}

		int availablePairs = 0;

		for(int first = 0; first < nodes.size(); first++) {
			for(int second = 0; second < nodes.size(); second++) {
				if((first != second) && (nodes.get(first).used != 0) && (nodes.get(first).used <= nodes.get(second).available)) {
					availablePairs++;
				}
			}
		}

		System.out.println(availablePairs);
	}

	public static void dailyPart2() {
		Scanner in = new Scanner(System.in);
		Node[][] nodesMatrix = new Node[28][32];

		for(int i = 0; i < INPUT_LENGTH; i++) {
			String[] separated = in.nextLine().split("\\s+");
			Node node = new Node().setAddress(separated[0])
								.setCapacity(Integer.valueOf(separated[1].substring(0, separated[1].length()-1)))
								.setUsed(Integer.valueOf(separated[2].substring(0, separated[2].length()-1)))
								.setAvailable(Integer.valueOf(separated[3].substring(0, separated[3].length()-1)))
								.setUsePercentage(Integer.valueOf(separated[4].substring(0, separated[4].length()-1)));

			nodesMatrix[node.y][node.x] = node;
		}

		for(Node[] nodesRow : nodesMatrix) {
			for(Node node : nodesRow) {
				if(node.x == 0 && node.y == 0) System.out.print('S');
				else if(node.x == 31 && node.y == 0) System.out.print('G');
				else if(node.used > 120) System.out.print('#');
				else if(node.used == 0) System.out.print('_');
				else System.out.print('.');
				System.out.print("\t");
			}
			System.out.println();
		}
	}

}
