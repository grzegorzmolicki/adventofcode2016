package com.molicki.day4;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DayFour {

	private static final int ENCRYPTED_NAME = 0;
	private static final int VALUE = 1;
	private static final int CHECKSUM = 2;

	private static boolean isRealRoom(Map<Integer, String> keys) {

		String sorted = keys.get(ENCRYPTED_NAME).chars()
									 .filter(c -> c != '-')
									 .mapToObj(c -> String.valueOf((char) c))
									 .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
									 .entrySet()
									 .stream()
									 .sorted((a, b) -> {
										 if (a.getValue().equals(b.getValue())) {
											 return a.getKey().compareTo(b.getKey());
										 } else {
											 return b.getValue().intValue() - a.getValue().intValue();
										 }
									 })
									 .limit(5)
									 .map(Map.Entry::getKey)
									 .collect(Collectors.joining());

		return sorted.equals(keys.get(CHECKSUM));
	}

	private static Map<Integer, String> splitIntoKeys(String input) {
		Map<Integer, String> keys = new HashMap<>();

		int lastIndex = input.lastIndexOf('-');

		keys.put(ENCRYPTED_NAME, input.substring(0, lastIndex));
		keys.put(VALUE, input.substring(lastIndex+1, input.indexOf('[')));
		keys.put(CHECKSUM, input.substring(input.indexOf('[') + 1, input.indexOf(']')));

		return keys;
	}

	public static void dailyPart1() {
		long sum = 0;
		Scanner in = new Scanner(System.in);

		for (int i = 0; i < 974; i++) {
			Map<Integer, String> keys = splitIntoKeys(in.nextLine());
			if (isRealRoom(keys)) {
				sum += Integer.valueOf(keys.get(VALUE));
			}
		}

		System.out.println("sum: " + sum);
	}

	public static void dailyPart2() {
		Scanner in = new Scanner(System.in);
		for (int i = 0; i < 974; i++) {
			Map<Integer, String> keys = splitIntoKeys(in.nextLine());

			if (isRealRoom(keys)) {
				int caesar = Integer.valueOf(keys.get(VALUE)) % 26;
				char[] chars = keys.get(ENCRYPTED_NAME).toCharArray();

				for (int iterator = 0; iterator < chars.length; iterator++) {
					if (chars[iterator] == '-') {
						chars[iterator] = ' ';
					} else {
						for (int c = 0; c < caesar; c++) {
							chars[iterator]++;
							if (chars[iterator] > 'z') {
								chars[iterator] = 'a';
							}

						}
					}

					if(String.valueOf(chars).startsWith("northpole")) {
						System.out.println("ID: " + keys.get(VALUE));
						return;
					}
				}
			}
		}
	}
}
