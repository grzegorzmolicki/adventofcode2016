package com.molicki.day6;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DaySix {

	public static void dailyPart1() throws IOException {
		Scanner in = new Scanner(System.in);
		List<List<Character>> characters = new ArrayList<>();

		for(int i = 0; i < 8; i++) {
			characters.add(new ArrayList<>());
		}

		for(int row = 0; row < 546; row++) {
			String line = in.nextLine();
			for(int i = 0; i < 8; i++) {
				characters.get(i).add(line.charAt(i));
			}
		}

		characters.forEach(column ->
			System.out.print(column.stream()
									 .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
									 .entrySet()
									 .stream()
									 .max(Comparator.comparing(Map.Entry::getValue))
									 .map(Map.Entry::getKey)
									 .get())
				  );
	}

	public static void dailyPart2() throws IOException {
		Scanner in = new Scanner(System.in);
		List<List<Character>> characters = new ArrayList<>();

		for(int i = 0; i < 8; i++) {
			characters.add(new ArrayList<>());
		}

		for(int row = 0; row < 546; row++) {
			String line = in.nextLine();
			for(int i = 0; i < 8; i++) {
				characters.get(i).add(line.charAt(i));
			}
		}

		characters.forEach(column ->
				System.out.print(column.stream()
									   .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
									   .entrySet()
									   .stream()
									   .min(Comparator.comparing(Map.Entry::getValue))
									   .map(Map.Entry::getKey)
									   .get())
		);
	}
}
