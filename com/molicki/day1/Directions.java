package com.molicki.day1;

public class Directions {
	private static final Point[] dirs = new Point[]{new Point(0, -1), new Point(1, 0), new Point(0, 1), new Point(-1, 0)};
	private int counter;

	public Directions() {
		this.counter = 0;
	}

	public void nextDir() {
		counter++;
		if(counter > 3) {
			counter = 0;
		}
	}

	public void prevDir() {
		counter--;
		if(counter < 0) {
			counter = 3;
		}
	}

	public int getXMultiplier() {
		return dirs[counter].xDir;
	}

	public int getYMultiplier() {
		return dirs[counter].yDir;
	}
}
