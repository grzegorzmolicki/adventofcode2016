package com.molicki.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DayOne {
	public static void dailyPart1() {
		Scanner in = new Scanner(System.in);
		Directions dirs = new Directions();
		int xPos = 0;
		int yPos = 0;
		String input;
		while(in.hasNext()) {
			input = in.next().replace(",", "");
			String directionRequest = input.substring(0, 1);
			Integer value = Integer.valueOf(input.substring(1));
			if(directionRequest.equals("R")) {
				dirs.nextDir();
			} else {
				dirs.prevDir();
			}
			xPos += dirs.getXMultiplier()*value;
			yPos += dirs.getYMultiplier()*value;
			System.out.println("X Y" + xPos + " " + yPos);
		}
	}

	public static void dailyPart2() {
		Scanner in = new Scanner(System.in);
		Directions dirs = new Directions();
		int xPos = 0;
		int yPos = 0;
		List<Point> visittedPoints = new ArrayList<>();

		String input;
		while(in.hasNext()) {
			input = in.next().replace(",", "");
			String directionRequest = input.substring(0, 1);
			Integer value = Integer.valueOf(input.substring(1));

			if (directionRequest.equals("R")) {
				dirs.nextDir();
			} else {
				dirs.prevDir();
			}

			while(value > 0) {
				xPos += dirs.getXMultiplier();
				yPos += dirs.getYMultiplier();
				for(Point p : visittedPoints) {
					if(p.xDir == xPos && p.yDir == yPos) {
						System.out.println("Was here on points: " + xPos + "  " + yPos + " distance: " + (Math.abs(xPos) + Math.abs(yPos)));
						return;
					}
				}
				visittedPoints.add(new Point(xPos, yPos));
				value--;
			}
		}
	}
}
