package com.molicki.day1;

public class Point {
	int xDir;
	int yDir;

	public Point(int x, int y) {
		this.xDir = x;
		this.yDir = y;
	}

	public int getX() {
		return xDir;
	}

	public int getY() {
		return yDir;
	}
}
