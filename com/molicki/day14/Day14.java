package com.molicki.day14;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day14 {
    private static final Pattern triple = Pattern.compile("(.)\\1\\1");
    private static Map<Long, String> hMap = new HashMap<>();

    private static MessageDigest md;

    private static boolean check1000(String preArranged, long counter, char c) {
        String test = String.valueOf(new char[]{c, c, c, c, c, c, c});
        for (int i = 1; i <= 100000; i++) {
            if (hMap.containsKey(counter + i)) {
                if (hMap.get(counter + i).contains(test))
                    return true;
            } else {
                String hash = getHashBuilder(preArranged + (counter + i)).toString();
                hMap.put(counter + i, hash);

                if (hash.contains(test))
                    return true;
            }
        }
        return false;
    }

    private static boolean check1000Part2(String preArranged, long counter, char c) {
        String test = String.valueOf(new char[]{c, c, c, c, c});
        for (long i = 1; i <= 1000; i++) {
            if (hMap.containsKey(counter + i)) {
                if (hMap.get(counter + i).contains(test))
                    return true;
            } else {
                String hash = generateHashPart2(getHashBuilder(preArranged + (counter + i)).toString(), 0);
                hMap.put(counter + i, hash);

                if (hash.contains(test))
                    return true;
            }
        }
        return false;
    }

    private static StringBuilder getHashBuilder(String inputString) {
        md.update((inputString).getBytes());
        byte[] data = md.digest();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            sb.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb;
    }

    public static void dailyPart1() throws NoSuchAlgorithmException {
        long startTime = System.currentTimeMillis();
        md = MessageDigest.getInstance("MD5");
        long start = 0L;
        int hashes = 0;
        String preArranged = "ahsbgdzn";

        Pattern triple = Pattern.compile("(.)\\1\\1\\1");

        while (hashes < 512) {

            StringBuilder sb = getHashBuilder(preArranged + start);
            Matcher matcher = triple.matcher(sb.toString());

            if (matcher.find()) {
                if (check1000(preArranged, start, sb.charAt(matcher.start()))) {
                    hashes++;
                }
            }
            start++;
        }
        System.out.println("Index: " + (start - 1));
        System.out.println("Took: " + (System.currentTimeMillis() - startTime));
    }


    public static String generateHashPart2(String hash, int counter) {
        if (counter == 2016) {
            return hash;
        }
        return generateHashPart2(getHashBuilder(hash).toString(), counter + 1);
    }

    public static void dailyPart2() throws NoSuchAlgorithmException {
        long startTime = System.currentTimeMillis();
        md = MessageDigest.getInstance("MD5");
        long start = 0L;

        int hashes = 0;
        String preArranged = "ahsbgdzn";

        while (hashes < 64) {
            String hash;
            if (hMap.containsKey(start)) {
                hash = hMap.get(start);
            } else {
                hash = generateHashPart2(preArranged + start, 0);
                hMap.put(start, hash);
            }

            Matcher matcher = triple.matcher(hash);
            if (matcher.find()) {
                if (check1000Part2(preArranged, start, hash.charAt(matcher.start()))) {
                    hashes++;
                }
            }
            start++;
        }
        System.out.println("LastHashIndex: " + (start - 1));
        System.out.println("Took: " + (System.currentTimeMillis() - startTime));
    }
}
