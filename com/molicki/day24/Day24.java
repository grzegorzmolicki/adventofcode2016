package com.molicki.day24;

import com.molicki.day1.Point;
import com.molicki.day13.Node;
import javafx.util.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Day24 {

    private static final int MAZE_ROWS = 39;
        private static final int MAZE_COLUMNS = 179;

    private static boolean isValid(char c) {
        return c != '#';
    }

    private static boolean contains(int x, int y, List<Node> nodes) {
        return nodes.stream()
                    .anyMatch(node -> node.getX() == x && node.getY() == y);
    }

    private static Node getFromXY(int x, int y, List<Node> openNodes) {
        return openNodes.stream()
                        .filter(node -> node.getX() == x && node.getY() == y)
                        .findFirst()
                        .get();
    }

    private static boolean isFinalNode(Node node, int finalX, int finalY) {
        return node.getX() == finalX && node.getY() == finalY;
    }

    private static void parseNodes(int x, int y, char c, Node currentNode, List<Node> openNodes, int finalX, int finalY) {
        if (isValid(c)) {
            if (!openNodes.isEmpty()) {
                if (contains(x, y, openNodes)) {
                    Node node = getFromXY(x, y, openNodes);
                    if (node.getGValue() > (currentNode.getGValue() + 1)) {
                        node.setGValue(currentNode.getGValue() + 1);
                        node.setFather(currentNode);
                    }
                } else {
                    Node newNode = new Node(x, y, true, currentNode);
                    newNode.setGValue(currentNode.getGValue() + 1);
                    newNode.setHValue(Math.abs(finalX - x) + Math.abs(finalY - y));
                    openNodes.add(newNode);
                }
            }
        }
    }

        private static void parseNode(Node node, Node finalNode, char[][] maze, List<Node> openNodes) {
            int x = node.getX() - 1;
            int y = node.getY();
            parseNodes(x, y, maze[x][y], node, openNodes, finalNode.getX(), finalNode.getY());

            x = node.getX() + 1;
            y = node.getY();
            parseNodes(x, y, maze[x][y], node, openNodes, finalNode.getX(), finalNode.getY());

            x = node.getX();
            y = node.getY() - 1;
            parseNodes(x, y, maze[x][y], node, openNodes, finalNode.getX(), finalNode.getY());

            x = node.getX();
            y = node.getY() + 1;
            parseNodes(x, y, maze[x][y], node, openNodes, finalNode.getX(), finalNode.getY());
    }
    private static int shortestPath(Node start, Node end, char[][] maze) {
        Node currentNode = start;
        List<Node> openNodes = new ArrayList<>();
        List<Node> closedNodes = new ArrayList<>();
        currentNode.setGValue(0);
        currentNode.setHValue(Math.abs(end.getX() - 1) + Math.abs(end.getY() - 1));
        openNodes.add(currentNode);

        do {
            parseNode(currentNode, end, maze, openNodes);
            if(!openNodes.isEmpty()) {
                if(openNodes.size() == 1) {
                    openNodes.remove(currentNode);
                    currentNode = openNodes.get(0);
                    closedNodes.add(currentNode);

                } else {
                    int index = 0;
                    for(int i = 1; i < openNodes.size(); i++) {
                        if(openNodes.get(i).getFValue() < openNodes.get(index).getFValue())
                            index = i;
                    }
                    Node temp = openNodes.get(index);
                    openNodes.remove(currentNode);
                    currentNode = temp;
                    closedNodes.add(currentNode);
                }
            }
        } while (!isFinalNode(currentNode, end.getX(), end.getY()) && !openNodes.isEmpty());

        int steps = 0;

        while(currentNode != null) {
            steps++;
            currentNode = currentNode.getFather();
        }
        return steps;
    }

    private static void putDistance(int start, int end, int length, Map<Pair<Integer, Integer>, Integer> distances) {
        distances.put(new Pair<>(start, end), length);
    }

    private static int[] charArrayToIntegersArray(char[] chars) {
        int[] integers = new int[chars.length];

        for(int i = 0; i < chars.length; i++) {
            integers[i] = chars[i]-48;
        }
        return integers;
    }

    private static int calculatePath(int[][] distances, int[] path) {
        int totalLength = 0;
        for(int i = 0; i < path.length-1; i++) {
            totalLength += distances[path[i]][path[i+1]];
        }
        return totalLength;
    }

    public static void dailyPart1() throws FileNotFoundException {
        char[][] maze = new char[MAZE_COLUMNS][MAZE_ROWS];
        Scanner in = new Scanner(System.in);
        Point[] points = new Point[8];

        for(int i = 0; i < MAZE_ROWS; i++) {
            char[] line = in.nextLine().toCharArray();
            for (int k = 0; k < MAZE_COLUMNS; k++) {
                maze[k][i] = line[k];
                if (maze[k][i] != '.' && maze[k][i] != '#') {
                    System.out.println("maze(" + k + "," + i + "): " + maze[k][i]);
                    points[(int) maze[k][i]-48] = new Point(k, i); // k as x/column, i as y/row
                }
            }
        }

        int[][] distances = new int[8][8];

        for(int i = 0; i < 7; i++) {
            for(int j = i+1; j < 8; j++) {
                    int length = shortestPath(new Node(points[i].getX(), points[i].getY(), false, null), new Node(points[j].getX(), points[j].getY(), false, null), maze);
                distances[i][j] = length;
                distances[j][i] = length;
                System.out.println("Distance from: " + i + " to " + j + ": " + length);
            }
        }

        File permutationsInput = new File("C:\\Users\\Grzegorz\\Documents\\java\\AdventOfCode\\day24permutations.txt");
        in = new Scanner(permutationsInput);
        int shortest = -1;
        while(in.hasNextLine()) {
            int[] path = charArrayToIntegersArray(in.nextLine().toCharArray());
            int length = calculatePath(distances, path);
            if(shortest == -1) {
                shortest = length;
            }
            if(length < shortest) {
                shortest = length;
            }
        }
        System.out.println(shortest-8);

    }
}
