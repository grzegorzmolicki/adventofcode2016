package com.molicki.day13;

public class Node {
	private int x;
	private int y;
	private Node father;
	private boolean wasCounted;
	private int gValue;
	private int hValue;

	public Node(int x, int y, boolean wasCounted, Node father) {
		this.x = x;
		this.y = y;
		this.wasCounted = wasCounted;
		this.father = father;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Node getFather() {
		return father;
	}

	public boolean isWasCounted() {
		return wasCounted;
	}

	public int getFValue() {
		return gValue+hValue;
	}

	public int getGValue() {
		return gValue;
	}

	public void setGValue(int gValue) {
		this.gValue = gValue;
	}

	public void setHValue(int hValue) {
		this.hValue = hValue;
	}

	public void setFather(Node node) {
		this.father = node;
	}

}
