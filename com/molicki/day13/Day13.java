package com.molicki.day13;

import java.util.*;

public class Day13 {

	private static final int finalX = 31;
	private static final int finalY = 39;

	private static List<Node> openNodes = new ArrayList<>();
	private static List<Node> closedNodes = new ArrayList<>();

	private static boolean isValid(int x, int y) {
		if (x < 0 || y < 0) {
			return false;
		}

		Optional<Node> fromOld = closedNodes.stream()
											.filter(node -> node.getX() == x && node.getY() == y)
											.findFirst();

		if (fromOld.isPresent())
			return false;

		long bits = Long.toBinaryString(x * x + 3 * x + 2 * x * y + y + y * y + 1352)
						.chars()
						.filter(c -> c == '1')
						.count();
		return (bits&1) == 0;
	}

	private static boolean isOpen(int x, int y) {
		return (Integer.toBinaryString(x * x + 3 * x + 2 * x * y + y + y * y + 1352)
					  .chars()
					  .filter(c -> c == '1')
					  .count()&1) == 0;
	}

	private static boolean contains(int x, int y, List<Node> nodes) {
		return nodes.stream()
					.anyMatch(node -> node.getX() == x && node.getY() == y);
	}

	private static Node getFromXY(int x, int y) {
		return openNodes.stream()
						.filter(node -> node.getX() == x && node.getY() == y)
						.findFirst()
						.get();
	}

	private static boolean isFinalNode(Node node) {
		return node.getX() == finalX && node.getY() == finalY;
	}

	private static void parseNodes(int x, int y, Node currentNode) {
		if (isValid(x, y)) {
			if (!openNodes.isEmpty()) {
				if (contains(x, y, openNodes)) {
					Node node = getFromXY(x, y);
					if (node.getGValue() > (currentNode.getGValue() + 1)) {
						node.setGValue(currentNode.getGValue() + 1);
						node.setFather(currentNode);
					}
				} else {
					Node newNode = new Node(x, y, true, currentNode);
					newNode.setGValue(currentNode.getGValue() + 1);
					newNode.setHValue(Math.abs(finalX - x) + Math.abs(finalY - y));
					openNodes.add(newNode);
				}
			}
		}
	}

	private static void parseNodesPart2(int x, int y, Node currentNode) {
		if (isValid(x, y)) {
			if (!openNodes.isEmpty()) {
				if (contains(x, y, openNodes)) {
					Node node = getFromXY(x, y);
					if (node.getGValue() > (currentNode.getGValue() + 1)) {
						node.setGValue(currentNode.getGValue() + 1);
						node.setFather(currentNode);
						parseNodePart2(node);
					}
				} else {
					Node newNode = new Node(x, y, true, currentNode);
					newNode.setGValue(currentNode.getGValue() + 1);
					newNode.setHValue(Math.abs(finalX - x) + Math.abs(finalY - y));
					openNodes.add(newNode);
					parseNodePart2(newNode);
				}
			}
		}
	}

	private static void parseNode(Node node) {
		int x = node.getX() - 1;
		int y = node.getY();
		parseNodes(x, y, node);

		x = node.getX() + 1;
		y = node.getY();
		parseNodes(x, y, node);

		x = node.getX();
		y = node.getY() - 1;
		parseNodes(x, y, node);

		x = node.getX();
		y = node.getY() + 1;
		parseNodes(x, y, node);
	}

	private static void parseNodePart2(Node node) {
		int x = node.getX() - 1;
		int y = node.getY();
		parseNodesPart2(x, y, node);

		x = node.getX() + 1;
		y = node.getY();
		parseNodesPart2(x, y, node);

		x = node.getX();
		y = node.getY() - 1;
		parseNodesPart2(x, y, node);

		x = node.getX();
		y = node.getY() + 1;
		parseNodesPart2(x, y, node);

	}

	private static char[][] createGrid(int width, int height) {
		char[][] grid = new char[height][width];
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
				if(x == finalX && y == finalY) grid[y][x] = 'T';
				else {
					if(isOpen(x, y)) grid[y][x] = ' ';
					else grid[y][x] = '#';
				}
			}
		}
		return grid;
	}

	private static void drawGrid(char[][] grid) {
		for(char[] line : grid) {
			for(char c : line) {
				System.out.print(c);
			}
			System.out.println();
		}
	}

	public static void dailyPart1() {

		long startMillis = System.currentTimeMillis();
		Node currentNode = new Node(1, 1, true, null);
		currentNode.setGValue(0);
		currentNode.setHValue(Math.abs(finalX - 1) + Math.abs(finalY - 1));
		openNodes.add(currentNode);

		do {
			parseNode(currentNode);
			if(!openNodes.isEmpty()) {
				if(openNodes.size() == 1) {
					openNodes.remove(currentNode);
					currentNode = openNodes.get(0);
					closedNodes.add(currentNode);

				} else {
					int index = 0;
					for(int i = 1; i < openNodes.size(); i++) {
						if(openNodes.get(i).getFValue() < openNodes.get(index).getFValue())
							index = i;
					}
					Node temp = openNodes.get(index);
					openNodes.remove(currentNode);
					currentNode = temp;
					closedNodes.add(currentNode);
				}
			}
		} while (!isFinalNode(currentNode) && !openNodes.isEmpty());

		System.out.println("Took: " + (System.currentTimeMillis() - startMillis));
		char[][] grid = createGrid(60, 60);

		int steps = 0;

		while(currentNode != null) {
			steps++;
			if(grid[currentNode.getY()][currentNode.getX()] != 'T') {
				char escCode = 0x1B;
				int row = 10; int column = 10;
				System.out.print(String.format("%c[%d;%df",escCode,currentNode.getY(),currentNode.getX()));
			}

				grid[currentNode.getY()][currentNode.getX()] = '\u2022';
			currentNode = currentNode.getFather();
		}

		//drawGrid(grid);

		System.out.println("Steps: " + steps);
	}

	public static void dailyPart2() {
		Node currentNode = new Node(1, 1, true, null);
		currentNode.setGValue(0);
		currentNode.setHValue(Math.abs(finalX - 1) + Math.abs(finalY - 1));
		openNodes.add(currentNode);
		parseNodePart2(currentNode);

		char[][] grid = createGrid(60, 60);

		openNodes.forEach(n -> {
			if(n.getX() < 60 && n.getY() < 60) {
				grid[n.getY()][n.getX()] = 'O';
			}
		});

		drawGrid(grid);

		long counter = openNodes.stream()
								.filter(node -> node.getGValue() < 51)
								.count();
		System.out.println("Tiles: " + counter);
	}
}

