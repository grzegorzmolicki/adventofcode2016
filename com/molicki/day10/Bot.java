package com.molicki.day10;

import java.util.*;

public class Bot {
    private static final int LOW = 0;
    private static final int HIGH = 1;

    Integer[] values = new Integer[2];
    Queue<String> requests = new LinkedList<>();
    Map<Integer, Bot> allBots;
    Map<Integer, OutputBin> outputBins;
    int id;
    boolean applyImmediately = false;

    public Bot(int id, Map<Integer, Bot> bots, Map<Integer, OutputBin> bins) {
        this.id = id;
        this.allBots = bots;
        this.outputBins = bins;
    }

    public void addRequest(String request) {
        this.requests.add(request);
        if(applyImmediately) {
            this.applyRequest();
            applyImmediately = false;
            return;
        }

        if(values[0] != null && values[1] != null) {
            this.applyRequest();
        }
    }

    public void addValue(Integer value) {
        if(values[0] == null) {
            values[0] = value;
        } else {
            values[1] = value;
        }
        if(values[0] != null && values[1] != null) {
            Arrays.sort(values);
            if(values[0].equals(17) && values[1].equals(61)) {
                System.out.println("Part1: " + id);
            }
            this.applyRequest();
        }
    }

    private void applyRequest() {
        if(!requests.isEmpty()) {
            String[] separated = requests.poll().split(" ");
            Bot destinationBot;
            int id = Integer.valueOf(separated[3]);

            if (separated[2].equals("bot")) {
                destinationBot = allBots.getOrDefault(id, new Bot(id, allBots, outputBins));
                destinationBot.addValue(values[LOW]);
                allBots.putIfAbsent(destinationBot.id, destinationBot);
            } else {
                outputBins.put(id, new OutputBin(id, values[LOW]));
            }

            id = Integer.valueOf(separated[8]);
            if (separated[7].equals("bot")) {
                destinationBot = allBots.getOrDefault(id, new Bot(id, allBots, outputBins));
                destinationBot.addValue(values[HIGH]);
                allBots.putIfAbsent(id, destinationBot);
            } else {
                outputBins.put(id, new OutputBin(id, values[HIGH]));
            }

            values[LOW] = null;
            values[HIGH] = null;
        } else {
            applyImmediately = true;
        }
    }
}
