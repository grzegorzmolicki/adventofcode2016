package com.molicki.day10;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class DayTen {

    public static void daily() {
        Map<Integer, Bot> bots = new TreeMap<>();
        Map<Integer, OutputBin> bins = new TreeMap<>();
        Scanner in = new Scanner(System.in);

        String line;
        for(int i = 0; i < 231; i++) {
            line = in.nextLine();
            String[] separated = line.split(" ");
            if(separated[0].equals("bot")) { // Request is that bot gives a value to...
                Bot bot = bots.getOrDefault(Integer.valueOf(separated[1]), new Bot(Integer.valueOf(separated[1]), bots, bins));
                bot.addRequest(line.substring(line.indexOf('l')));
                bots.putIfAbsent(bot.id, bot);
            } else { // Request is that bot gets a value
                Bot bot = bots.getOrDefault(Integer.valueOf(separated[5]), new Bot(Integer.valueOf(separated[5]), bots, bins));
                bot.addValue(Integer.valueOf(separated[1]));
                bots.putIfAbsent(bot.id, bot);
            }
        }
        System.out.println("Part2: " + bins.get(0).value*bins.get(1).value*bins.get(2).value);
    }
}
