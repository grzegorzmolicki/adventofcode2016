package com.molicki.day25;

import java.util.*;

public class Day25 {

    private static final int INPUT_LENGTH = 30;

    private static boolean isDigit(String s) {
        try {
            Integer.valueOf(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean isClockSignal(List<Integer> clockSignal) {
        for (int i = 0; i < clockSignal.size(); i += 2) {
            if (clockSignal.get(i) % 2 != 0) {
                return false;
            }
        }

        for (int i = 1; i < clockSignal.size(); i += 2) {
            if (clockSignal.get(i) % 2 != 1) {
                return false;
            }
        }
        return true;
    }

    public static void daily() {
        ArrayList<String> code = new ArrayList<>(INPUT_LENGTH);
        Scanner in = new Scanner(System.in);

        for (int i = 0; i < INPUT_LENGTH; i++) {
            code.add(in.nextLine());
        }

        boolean found = false;
        int initialValue = 1;

        while (!found) {
            List<Integer> clockSignal = new ArrayList<>(50);
            System.out.println("Testing: " + initialValue);

            Map<Character, Integer> registers = new HashMap<>();
            registers.put('a', initialValue++);
            registers.put('b', 0);
            registers.put('c', 0);
            registers.put('d', 0);

            int lineCounter = 0;

            while (lineCounter < INPUT_LENGTH) {
                String[] separated = code.get(lineCounter).split(" ");
                Integer registerValue;
                switch (separated[0]) {
                    case "cpy":
                        String from = separated[1];
                        String to = separated[2];
                        int value;

                        if (from.length() == 1 && Character.isAlphabetic(from.charAt(0))) {
                            value = registers.get(from.charAt(0));
                        } else {
                            value = Integer.valueOf(from);
                        }

                        registers.put(to.charAt(0), value);
                        lineCounter++;
                        break;
                    case "inc":
                        registerValue = registers.get(separated[1].charAt(0));
                        registerValue++;
                        registers.put(separated[1].charAt(0), registerValue);
                        lineCounter++;
                        break;
                    case "dec":
                        registerValue = registers.get(separated[1].charAt(0));
                        registerValue--;
                        registers.put(separated[1].charAt(0), registerValue);
                        lineCounter++;
                        break;
                    case "jnz":
                        int index = 0;
                        if (isDigit(separated[1])) {
                            if (Integer.valueOf(separated[1]) != 0) {
                                if (isDigit(separated[2]))
                                    index = Integer.valueOf(separated[2]);
                                else
                                    index = registers.get(separated[2].charAt(0));
                            }
                        } else {
                            if (registers.get(separated[1].charAt(0)) != 0) {
                                if (isDigit(separated[2]))
                                    index = Integer.valueOf(separated[2]);
                                else {
                                    System.out.println(Arrays.toString(separated));
                                    index = registers.get(separated[2].charAt(0));
                                }
                            }
                        }

                        if (index != 0)
                            lineCounter += index;
                        else
                            lineCounter++;
                        break;
                    case "out":
                        if (isDigit(separated[1])) {
                            clockSignal.add(Integer.valueOf(separated[1]));
                        } else {
                            clockSignal.add(registers.get(separated[1].charAt(0)));
                        }
                        lineCounter++;
                        System.out.println("Adding: " + clockSignal.get(clockSignal.size() - 1));
                        break;
                }

                if (isClockSignal(clockSignal)) {
                    if (clockSignal.size() > 50) {
                        found = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            System.out.println("A Register: " + (initialValue - 1));
        }
    }
}
