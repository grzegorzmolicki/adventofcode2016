package com.molicki.day21;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Day21 {

    private static final int INPUT_LENGTH = 100;

    private static char[] swapXY(int x, int y, char[] origin) {
        char xChar = origin[x];
        origin[x] = origin[y];
        origin[y] = xChar;
        return origin;
    }

    private static char[] swapXY(char x, char y, char[] origin) {
        for(int i = 0; i < origin.length; i++) {
            if(origin[i] == x) {
                origin[i] = y;
            } else if(origin[i] == y) {
                origin[i] = x;
            }
        }
        return origin;
    }

    private static char[] rotateLeft(int n, char[] origin) {
        char[] newArray = new char[origin.length];
        n %= origin.length;

        for(int i = n; i < origin.length; i++) {
            newArray[i-n] = origin[i];
        }
        for(int i = 0; i < n; i++) {
            newArray[newArray.length-n+i] = origin[i];
        }

        return newArray;
    }

    private static char[] rotateRight(int n, char[] origin) {
        char[] newArray = new char[origin.length];

        for(int i = 0; i < origin.length; i++) {
            newArray[(i+n)%origin.length] = origin[i];
        }

        return newArray;
    }

    private static char[] moveFromTo(int from, int to, char[] origin) {
        List<Character> chars = new ArrayList<>(origin.length);
        for(char c : origin)
            chars.add(c);

        char cfrom = chars.get(from);
        chars.remove(from);
        chars.add(to, cfrom);

        for(int i = 0; i < chars.size(); i++) {
            origin[i] = chars.get(i);
        }

        return origin;
    }
    private static int getIndexOf(char c, char[] origin) {
        for(int i = 0; i < origin.length; i++) {
            if(origin[i] == c) {
                return i;
            }
        }
        return -1;
    }

    private static char[] reverse(int from, int to, char[] origin) {
        char[] cpy = Arrays.copyOfRange(origin, from, to+1);

        for(int i = 0; i < cpy.length; i++) {
            origin[to-i] = cpy[i];
        }

        return origin;
    }

    private static void moveCursorTo(int x, int y) {
        char escCode = 0x1B;
        System.out.print(String.format("%c[%d;%df",escCode,y,x));
    }

    public static void dailyPart1() throws FileNotFoundException, InterruptedException {
        char[] origin = new char[]{'f', 'b', 'g', 'd', 'c', 'e', 'a', 'h'};
        File input = new File("C:\\Users\\Grzegorz\\Documents\\java\\AdventOfCode\\day21.txt");
        Scanner in = new Scanner(input);
        moveCursorTo(4, 6);
        System.out.print(new String(origin));

        for(int row = 0; row < INPUT_LENGTH; row++) {
            Thread.sleep(100);
            String[] request = in.nextLine().split(" ");

            switch (request[0]) {
                case "swap":
                    switch (request[1]) {
                        case "position":
                            origin = swapXY(Integer.valueOf(request[2]), Integer.valueOf(request[5]), origin);
                            break;
                        case "letter":
                            origin = swapXY(request[2].charAt(0), request[5].charAt(0), origin);
                            break;
                    }
                    break;
                case "rotate":
                    switch (request[1]) {
                        case "left":
                            origin = rotateLeft(Integer.valueOf(request[2]), origin);
                            break;
                        case "right":
                            origin = rotateRight(Integer.valueOf(request[2]), origin);
                            break;
                        case "based":
                            int indexOf = getIndexOf(request[6].charAt(0), origin);
                            if(indexOf > 3)
                                indexOf++;
                            indexOf++;
                            origin = rotateRight(indexOf, origin);
                            break;
                    }
                    break;
                case "reverse":
                    origin = reverse(Integer.valueOf(request[2]), Integer.valueOf(request[4]), origin);
                    break;
                case "move":
                    origin = moveFromTo(Integer.valueOf(request[2]), Integer.valueOf(request[5]), origin);
                    break;
            }

            moveCursorTo(4, 6);
            System.out.print(new String(origin));
        }
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();

    }

    public static void dailyPart2() throws FileNotFoundException, InterruptedException {
        File input = new File("C:\\Users\\Grzegorz\\Documents\\java\\AdventOfCode\\day21.txt");
        File inputPerms = new File("C:\\Users\\Grzegorz\\Documents\\java\\AdventOfCode\\day21permutations.txt");

        List<String> permutations = new ArrayList<>(40320);
        List<String> requests = new ArrayList<>(100);
        Scanner in = new Scanner(inputPerms);

        for(int i = 0; i < 40320; i++) {
            permutations.add(in.nextLine());
        }

        in = new Scanner(input);

        for(int i = 0; i < 100; i++) {
            requests.add(in.nextLine());
        }

        long start = System.currentTimeMillis();

        for(int perm = 0; perm < 40320; perm++) {
            char[] origin = permutations.get(perm).toCharArray();
            moveCursorTo(3, 7);
            System.out.println(permutations.get(perm));
            for(int row = 0; row < INPUT_LENGTH; row++) {
                String[] request = requests.get(row).split(" ");

                moveCursorTo(3, 8);
                System.out.println(new String(origin));
                switch (request[0]) {
                    case "swap":
                        switch (request[1]) {
                            case "position":
                                origin = swapXY(Integer.valueOf(request[2]), Integer.valueOf(request[5]), origin);
                                break;
                            case "letter":
                                origin = swapXY(request[2].charAt(0), request[5].charAt(0), origin);
                                break;
                        }
                        break;
                    case "rotate":
                        switch (request[1]) {
                            case "left":
                                origin = rotateLeft(Integer.valueOf(request[2]), origin);
                                break;
                            case "right":
                                origin = rotateRight(Integer.valueOf(request[2]), origin);
                                break;
                            case "based":
                                int indexOf = getIndexOf(request[6].charAt(0), origin);
                                if(indexOf > 3)
                                    indexOf++;
                                indexOf++;
                                origin = rotateRight(indexOf, origin);
                                break;
                        }
                        break;
                    case "reverse":
                        origin = reverse(Integer.valueOf(request[2]), Integer.valueOf(request[4]), origin);
                        break;
                    case "move":
                        origin = moveFromTo(Integer.valueOf(request[2]), Integer.valueOf(request[5]), origin);
                        break;
                }
            }
        }
    }
}
