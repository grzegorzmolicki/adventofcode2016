package com.molicki.day19;

class Elf {
	private long id;

	public Elf(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}

		if(obj instanceof Elf) {
			Elf o = (Elf) obj;
			return o.getId() == this.id;

		} else {
			return false;
		}
	}
}
