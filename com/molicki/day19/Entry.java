package com.molicki.day19;

class Entry<E> {
	E value;
	Entry<E> prev;
	Entry<E> next;

	public Entry(E object) {
		this.value = object;
		prev = null;
		next = null;
	}
}
