package com.molicki.day19;

import java.util.NoSuchElementException;

public class DoublyCircularLinkedList<E> {
	private Entry<E> head;
	private long size;

	public DoublyCircularLinkedList() {
		this.size = 0;
		this.head = null;
	}

	public DoublyCircularLinkedList(E object) {
		this.head = new Entry<>(object);
		this.head.next = this.head;
		this.head.prev = this.head;
		this.size = 1;
	}

	public void add(E object) {
		if(this.head == null) {
			Entry<E> entry = new Entry<E>(object);
			this.head = entry;
			this.head.next = entry;
			this.head.prev = entry;
		} else {
			Entry<E> entry = new Entry<>(object);
			Entry<E> tail = this.head.prev;

			tail.next = entry;

			entry.prev = tail;
			entry.next = this.head;

			this.head.prev = entry;
		}

		this.size++;
	}

	public boolean remove(E object) {
		try {
			Entry<E> entry = getEntryByObject(object);
			entry.next.prev = entry.prev;
			entry.prev.next = entry.next;
			entry = null; // Let the GC claim that
			size--;
			return true;
		} catch (NoSuchElementException e) {
			System.out.println("No such element");
			System.out.println("Size: " + size);
			return false;
		}
	}

	public boolean remove(Entry<E> entry) {
		entry.next.prev = entry.prev;
		entry.prev.next = entry.next;
		size--;
		return true;
	}

	private Entry<E> getEntryByObject(E object) throws NoSuchElementException {
		Entry<E> currentEntry = head;
		do {
			if(currentEntry.value.equals(object)) {
				return currentEntry;
			}
			currentEntry = currentEntry.next;
		} while(currentEntry != head);

		throw new NoSuchElementException("No element");
	}

	public Entry<E> getHead() {
		return this.head;
	}

	public long size() {
		return size;
	}
}
