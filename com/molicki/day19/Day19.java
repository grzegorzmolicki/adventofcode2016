package com.molicki.day19;

public class Day19 {

	private static final long INPUT = 3012210L;

	public static void dailyPart1() {
		DoublyCircularLinkedList<Elf> circle = new DoublyCircularLinkedList<>();
		long start = System.currentTimeMillis();
		for (int i = 0; i < INPUT; i += 2) {
			circle.add(new Elf(i));
		}

		Entry<Elf> current = circle.getHead();
		while (true) {
			if (circle.size() == 1) {
				long end = System.currentTimeMillis();
				System.out.println("ID: " + (current.value.getId()+1));
				System.out.println("Took: " + (end-start));
				break;
			} else {
				circle.remove(current.next);
				current = current.next;
			}
		}
	}

	public static void dailyPart2() {
		DoublyCircularLinkedList<Elf> circle = new DoublyCircularLinkedList<>();
		long midId = INPUT/2;
		long start = System.currentTimeMillis();
		Entry<Elf> mid = null;
		for(int i = 0; i < INPUT; i++) {
			circle.add(new Elf(i));
			if(i == midId) {
				mid = circle.getHead().prev;
			}
		}

		Entry<Elf> current = circle.getHead();

		while(true) {
			if (circle.size() == 1) {
				long end = System.currentTimeMillis();
				System.out.println("Id: " + (current.value.getId()+1));
				System.out.println("Took: " + (end - start));
				break;
			} else {
				Entry<Elf> nextMid = mid.next;
				circle.remove(mid);
				mid = nextMid;
				if(circle.size()%2 == 0) {
					mid = mid.next;
				}
				current = current.next;
			}
		}
	}
}
