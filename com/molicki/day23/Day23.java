package com.molicki.day23;

import java.util.*;

public class Day23 {

    private static final int INPUT_LENGTH = 26;

    private static String parseCode(String code) {
        String[] separated = code.split(" ");
        if (separated.length == 2) {
            // single argument instructions;
            if (separated[0].equals("inc")) {
                separated[0] = "dec";
            } else {
                separated[0] = "inc";
            }
        } else {
            // double argument instruction
            if (separated[0].equals("jnz")) {
                separated[0] = "cpy";
            } else {
                separated[0] = "jnz";
            }
        }

        if (separated[0].equals("cpy") && separated[1].matches("\\d+") && separated[2].matches("\\d+")) {
            return code;
        }

        return Arrays.stream(separated)
                     .reduce((a, b) -> a + " " + b).get();
    }

    private static int parseIndex(String param, Map<Character, Integer> registers) {
        if(param.length() == 1 && param.charAt(0) >= 'a' && param.charAt(0) < 'e') {
            return registers.get(param.charAt(0));
        } else {
            return Integer.valueOf(param);
        }
    }

    private static boolean isNumber(String input) {
        return input.matches("\\d+");
    }

    public static void dailyPart1() {
        List<String> code = new ArrayList<>(INPUT_LENGTH);
        List<Integer> tglItselfCache = new ArrayList<>();
        Scanner in = new Scanner(System.in);

        int previousLine = 0;


        for (int i = 0; i < INPUT_LENGTH; i++) {
            code.add(in.nextLine());
        }

        Map<Character, Integer> registers = new HashMap<>();
        registers.put('a', 12);
        registers.put('b', 0);
        registers.put('c', 0);
        registers.put('d', 0);

        int lineCounter = 0;

        while (lineCounter < INPUT_LENGTH) {
            String[] separated = code.get(lineCounter).split(" ");
            Integer registerValue;

            if (tglItselfCache.contains(lineCounter)) {
                tglItselfCache.remove(lineCounter);
                code.set(lineCounter, parseCode(code.get(lineCounter)));
            }

            switch (separated[0]) {
                case "cpy":
                    String from = separated[1];
                    String to = separated[2];
                    int value;

                    if (from.length() == 1 && Character.isAlphabetic(from.charAt(0))) {
                        value = registers.get(from.charAt(0));
                    } else {
                        value = Integer.valueOf(from);
                    }

                    registers.put(to.charAt(0), value);
                    lineCounter++;
                    break;
                case "inc":
                    registerValue = registers.get(separated[1].charAt(0));
                    registerValue++;
                    registers.put(separated[1].charAt(0), registerValue);
                    lineCounter++;
                    break;
                case "dec":
                    registerValue = registers.get(separated[1].charAt(0));
                    registerValue--;
                    registers.put(separated[1].charAt(0), registerValue);
                    lineCounter++;
                    break;
                case "jnz":
                    if (separated[1].length() == 1 && separated[1].charAt(0) > 'a' && separated[1].charAt(0) < 'e') {
                        if (registers.get(separated[1].charAt(0)) != 0) {
                            lineCounter += Integer.valueOf(separated[2]);
                        } else {
                            lineCounter++;
                        }
                    } else {
                        if (Integer.valueOf(separated[1]) != 0) {
                            if(isNumber(separated[2]))
                                lineCounter += Integer.valueOf(separated[2]);
                            else
                                lineCounter += registers.get(separated[2].charAt(0));
                        } else {
                            lineCounter++;
                        }
                    }
                    break;
                case "tgl":
                    int index = parseIndex(separated[1], registers);
                    if(index == 0) {
                        tglItselfCache.add(lineCounter);
                    } else {
                        int destination = index + lineCounter;

                        if(destination > 0 && destination < code.size()) {
                            code.set(destination, parseCode(code.get(destination)));
                        }
                    }

                    lineCounter++;
                    break;
            }
        }
        System.out.println("A: " + registers.get('a'));
    }
}
