package com.molicki.day3;

import java.util.Arrays;
import java.util.Scanner;

public class DayThree {

	private static boolean isTriangle(int[] sides) {
		Arrays.sort(sides);
		return sides[2] < (sides[0] + sides[1]);
	}

	public static void dailyPart2() {
		Scanner in = new Scanner(System.in);
		int validTriangles = 0;
		int[][] triangles = new int[3][3];

		for(int i = 0; i < 1992; i += 3) {

			for(int y = 0; y < 3; y++) {
				for(int x = 0; x < 3; x++) {
					triangles[x][y] = in.nextInt();
				}
			}
			for(int triangle = 0; triangle < 3; triangle++) {
				if(isTriangle(triangles[triangle])) {
					validTriangles++;
				}
			}
		}
		System.out.println("Valid Triangles: " + validTriangles);
	}
}
