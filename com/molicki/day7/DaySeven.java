package com.molicki.day7;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class DaySeven {

	private static boolean stringMatchesPart1(String s) {
		for (int i = 0; i < s.length() - 3; i++) {
			if (s.charAt(i) != s.charAt(i + 1)
					&& s.charAt(i + 2) != s.charAt(i + 3)
					&& s.charAt(i) == s.charAt(i + 3)
					&& s.charAt(i + 1) == s.charAt(i + 2)) {
				return true;
			}
		}
		return false;
	}

	private static boolean addressSupportsTLS(String row) {
		Pair<List<Integer>, List<Integer>> openingClosing = getOpeningAndClosingBrackets(row);

		for (int i = 0; i < openingClosing.getKey().size(); i++) {
			if (stringMatchesPart1(row.substring(openingClosing.getKey().get(i), openingClosing.getValue().get(i) + 1))) {
				return false;
			}
		}

		for (int i = 0; i < row.length() - 3; i++) {
			if (stringMatchesPart1(row.substring(i, i + 4))) {
				return true;
			}
		}
		return false;
	}

	private static List<String> stringMatchesPart2(String s) {
		List<String> matches = new ArrayList<>();
		for (int i = 0; i < s.length() - 2; i++) {
			if(s.charAt(i) != s.charAt(i+1)
					&& s.charAt(i) == s.charAt(i+2)) {
				matches.add(s.substring(i, i+3));
			}
		}
		return matches;
	}

	private static Pair<List<Integer>, List<Integer>> getOpeningAndClosingBrackets(String row) {
		List<Integer> opening = new LinkedList<>();
		List<Integer> closing = new LinkedList<>();

		for (int i = 0; i < row.length(); i++) {
			if (row.charAt(i) == '[') opening.add(i);
			else if (row.charAt(i) == ']') closing.add(i);
		}
		return new Pair<>(opening, closing);
	}

	private static boolean stringMatchesReversedPattern(String row, String pattern) {
		String reversedPattern = "" + 	pattern.charAt(1) + pattern.charAt(0) + pattern.charAt(1);
		return row.contains(reversedPattern);
	}

	private static boolean addressSupportsSSL(String row) {
		Pair<List<Integer>, List<Integer>> openingClosing = getOpeningAndClosingBrackets(row);

		List<String> patterns = new ArrayList<>();
		for(int i = 0; i < openingClosing.getKey().size(); i++) {
			patterns.addAll(stringMatchesPart2(row.substring(openingClosing.getKey().get(i), openingClosing.getValue().get(i)+1)));
		}

		row = row.replaceAll("\\[\\w*\\]", "");

		for(String pattern : patterns) {
			if(stringMatchesReversedPattern(row, pattern)){
				return true;
			}
		}

		return false;
	}

	public static void dailyPart1() {
		Scanner in = new Scanner(System.in);

		long rowsCounter = 0;
		for (int row = 0; row < 2000; row++) {
			String address = in.nextLine();
			if (addressSupportsTLS(address)) {
				rowsCounter++;
			}
		}
		System.out.println("Rows matching: " + rowsCounter);
	}

	public static void dailyPart2() {
		Scanner in = new Scanner(System.in);

		long rowsCounter = 0;
		for (int row = 0; row < 2000; row++) {
			String line = in.nextLine();
			if (addressSupportsSSL(line)) {
				rowsCounter++;
			}
		}
		System.out.println("Rows matching: " + rowsCounter);
	}
}
