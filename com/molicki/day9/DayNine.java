package com.molicki.day9;

import java.util.Scanner;

public class DayNine {

	private static String decodeString(String line) {
		StringBuilder builder = new StringBuilder(line.length()*2);
		for(int i = 0; i < line.length(); i++) {
			if(line.charAt(i) == '(') {
				int indexOfClosing = line.indexOf(')', i);
				String[] bracketSeparated = line.substring(i+1, indexOfClosing).split("x");
				for(int counter = 0; counter < Integer.valueOf(bracketSeparated[1]); counter++) {
					builder.append(line.substring(indexOfClosing+1, Math.min(Integer.valueOf(bracketSeparated[0])+indexOfClosing+1, line.length())));
				}
				i = Integer.valueOf(bracketSeparated[0]) + indexOfClosing;
			} else {
				builder.append(line.charAt(i));
			}

		}

		return builder.toString();
	}

	private static long multiplierDecoder(String line, int multiplier) {
		long length = 0;
		if(line.contains("(")) {
			for(int i = 0; i < line.length(); i++) {
				if(line.charAt(i) != '(') {
					length++;
				} else {
					int closingIndex = line.indexOf(')', i);
					String[] brackets = line.substring(i+1, closingIndex).split("x");

					String toParse = line.substring(closingIndex+1, Integer.valueOf(brackets[0])+closingIndex+1);
					String left = line.substring(Integer.valueOf(brackets[0])+closingIndex+1);

					length += multiplierDecoder(toParse, Integer.valueOf(brackets[1]));
					length += multiplierDecoder(left, 1);
					break;
				}
			}
			return length*multiplier;
		} else {
			return line.length()*multiplier;
		}
	}

	public static void dailyPart1() {
		String line = new Scanner(System.in).nextLine();
		System.out.println(decodeString(line).length());
	}

	public static void dailyPart2() {
		String line = new Scanner(System.in).nextLine();
		System.out.println(multiplierDecoder(line, 1));
	}
}
