package com.molicki.day17;

import java.io.FileNotFoundException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.Queue;

public class Day17 {

    private static final String PUZZLE = "mmsxrhfx";
    private static NewNode END_NODE;
    private static MessageDigest md;

    private static String getMD5(String input) throws NoSuchAlgorithmException {
        md.update(input.getBytes());
        byte[] data = md.digest();

        StringBuilder sb = new StringBuilder(4);

        for(int i = 0; i < 4; i++) {
            sb.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    private static boolean isOpen(String hash, int direction, int x, int y) {
        if(x > 4 || y > 4) {
            return false;
        }

        return hash.charAt(direction) < 'g' && hash.charAt(direction) > 'a';
    }

    private static void parseNode(NewNode node, char[][] maze) throws NoSuchAlgorithmException {

    }

    public static void dailyPart1() throws FileNotFoundException, NoSuchAlgorithmException {
        NewNode start = new NewNode(1, 1, PUZZLE, "");
        md = MessageDigest.getInstance("MD5");


        END_NODE = new NewNode(4, 4, PUZZLE, "");

        Queue<NewNode> nodes = new LinkedList<>();
        nodes.add(start);
        int longest = 0;

        while(!nodes.isEmpty()) {
            NewNode node = nodes.poll();

            if(node.x == END_NODE.x && node.y == END_NODE.y) {
                if(node.getPuzle().length()-8 > longest) {
                    longest = node.getPuzle().length()-8;
                    System.out.println("Longest: " + longest);
                }
                continue;
            }

            String md5 = getMD5(node.getPuzle());

            if(isOpen(md5, 0, node.x, node.y-1)) {
                nodes.add(new NewNode(node.x, node.y-1, node.puzzle, "U"));
            }

            if(isOpen(md5, 1, node.x, node.y+1)) {
                nodes.add(new NewNode(node.x, node.y+1, node.puzzle, "D"));
            }

            if(isOpen(md5, 2, node.x-1, node.y)) {
                nodes.add(new NewNode(node.x-1, node.y, node.puzzle, "L"));
            }

            if(isOpen(md5, 3, node.x+1, node.y)) {
                nodes.add(new NewNode(node.x+1, node.y, node.puzzle, "R"));
            }
        }

        System.out.println("Longest: " + longest);
    }
}

