package com.molicki.day17;

public class NewNode {

    int x;
    int y;
    String path;
    String puzzle;

    public NewNode(int x, int y, String puzzle, String path) {
        this.x = x;
        this.y = y;
        this.path = path;
        this.puzzle = puzzle + path;
    }

    public NewNode() {
    }

    public String getPath() {
        return path;
    }

    public String getPuzle() {
        return puzzle;
    }

}
