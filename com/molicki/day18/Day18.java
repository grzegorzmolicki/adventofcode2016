package com.molicki.day18;

import java.util.Arrays;

public class Day18 {

    private static final String INPUT = "^.^^^..^^...^.^..^^^^^.....^...^^^..^^^^.^^.^^^^^^^^.^^.^^^^...^^...^^^^.^.^..^^..^..^.^^.^.^.......";
    private static final boolean TRAP = true;
    private static final boolean PLATE = false;


    private static int countPlates(boolean[] row) {
        int counter = 0;
        for(int i = 1; i < row.length-1; i++) {
            if(!row[i])
                counter++;
        }
        return counter;
    }


    public static void dailyPart1() {
        boolean[] prev = new boolean[INPUT.length()+2];
        prev[0] = false;
        prev[prev.length-1] = false;


        boolean[] current = new boolean[prev.length];
        for(int i = 0; i < INPUT.length(); i++) {
            if(INPUT.charAt(i) == '^') {
                prev[i+1] = true;
            } else {
                prev[i+1] = false;
            }
        }

        int numberOfPlates = countPlates(prev);
        int numberOfRows = 0;

        while(numberOfRows++ < (400000-1)) {
            for(int i = 1; i < prev.length-1; i++) {
                if(prev[i-1]^prev[i+1]) {
                    current[i] = TRAP;
                } else {
                    current[i] = PLATE;
                }
            }
            numberOfPlates += countPlates(current);
            prev = Arrays.copyOf(current, current.length);
        }
        System.out.println("Safe: " + numberOfPlates);
    }
}
